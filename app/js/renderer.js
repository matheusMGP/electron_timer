const { ipcRenderer } = require('electron');
const timer = require('./timer');
const data = require('../../data');

let linkSobre = document.querySelector('#link-sobre');
let botaoPlay = document.querySelector('.botao-play');
let tempo = document.querySelector('.tempo');
let curso = document.querySelector('.curso');
let botaoAdicionar = document.querySelector('.botao-adicionar');
let campoAdicionar = document.querySelector('.campo-adicionar');

let imgs = ['img/play-button.svg', 'img/stop-button.svg'];
let play = false;

botaoAdicionar.addEventListener('click', function() {
    let novoCurso = campoAdicionar.value;
    if( novoCurso > ''){
        curso.textContent = novoCurso;
        tempo.textContent = '00:00:00';
        campoAdicionar.value = '';
        ipcRenderer.send('curso-adicionado',novoCurso);//esse evento vai pro main.js
    }else
       alert('Digite o nome do curso.');
    
});


window.onload = function(){
    if (curso.textContent != 'Nenhum-curso-selecionado'){
        data.pegaDados(curso.textContent)
        .then( (dados) => {
            tempo.textContent = dados.tempoEstudo;           
        });
    }
   
}

linkSobre.addEventListener('click' , function(){
    ipcRenderer.send('abrir-janela-sobre');
});

botaoPlay.addEventListener('click' , function(){
    if(curso.textContent !='Nenhum-curso-selecionado'){
        if (play){       
            timer.parar(curso.textContent);
            play= false;
            new Notification('Tempo de estudo parado',{
                body:`O curso ${curso.textContent} foi parado!!`,
                icon:'img/play-button.svg'
            });
        }else {
           
                console.log('entrou');
                timer.iniciar(tempo);
                play= true;
                new Notification('Tempo de estudo iniciado',{
                    body:`O curso ${curso.textContent} foi iniciado!!`,
                    icon:'img/play-button.svg'
                });
            }
        posicao = imgs.reverse();
        botaoPlay.src = imgs[0];

    }else
      alert('Nenhum curso selecionado.');
       
    

    
});


ipcRenderer.on('curso-trocado', (event,cursoSelecionado) => {
   timer.parar(curso.textContent);  
    
    data.pegaDados(cursoSelecionado).then( (dados) => {
        tempo.textContent = dados.tempoEstudo;  
    });
    curso.textContent = cursoSelecionado;
})

ipcRenderer.on('inicia-timer-atalho', () => {
    //botaoPlay.click();
   let click = new MouseEvent('click');
   botaoPlay.dispatchEvent(click);

  
});
