const { app, BrowserWindow, ipcMain ,Tray, Menu, globalShortcut,autoUpdater} = require('electron');
const data = require('./data');
const templateGenerator = require('./template');

let tray = null;
let mainWindow = null;
let sobreWindow = null;

app.on('ready', () => {
   mainWindow = new BrowserWindow({
      title: 'Berp Timer',
      width: 600,
      height:400
  });

  //app.setAppUserModelId(process.execPath); 

  tray = new Tray(__dirname + '/app/img/icon-tray.png');
  let template = templateGenerator.geraTrayTemplate(mainWindow);
  let trayMenu = Menu.buildFromTemplate(template);
  tray.setContextMenu(trayMenu);

  let menuPrincipal = templateGenerator.criaMenuPrincipal(mainWindow);
  let templateMenu = Menu.buildFromTemplate(menuPrincipal);
  Menu.setApplicationMenu(templateMenu);

  globalShortcut.register('CmdOrCtrl+Shift+P', () => {
    mainWindow.send('inicia-timer-atalho');
  });

  //mainWindow.openDevTools();//para abrir o f12 para desenvolvedor
  mainWindow.loadURL(`file://${__dirname}/app/index.html`);
});


app.on('window-all-closed', () => {
  app.quit();  
});


ipcMain.on('abrir-janela-sobre', () => {
    if (sobreWindow == null) {
        sobreWindow = new BrowserWindow({
        title: 'Sobre',
        width: 300,
        height:220,
        alwaysOnTop: true,
        frame: false      
      });
        sobreWindow.on('closed', () => {
           sobreWindow = null;
        });
      }  
    sobreWindow.loadURL(`file://${__dirname}/app/sobre.html`);
});

ipcMain.on('fechar-janela-sobre', () => { 
  sobreWindow.close();
});

ipcMain.on('curso-parado', (event,curso,tempoEstudado) => { 
  data.salvaDados(curso ,tempoEstudado);
});

ipcMain.on('curso-adicionado',(event,novoCurso) => {
  let novoTemplate = templateGenerator.adicionaCursoNoTray(novoCurso,mainWindow);
  let novoTrayMenu = Menu.buildFromTemplate(novoTemplate);
  tray.setContextMenu(novoTrayMenu);
  data.salvaDados(novoCurso ,'00:00:00');
})