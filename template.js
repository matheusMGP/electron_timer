const data = require('./data');
const { ipcMain} = require('electron');
module.exports = {   
    templateInicial :null,
  
    geraTrayTemplate(mainWindow){
        let template = [
            {
                label: 'Cursos'
            },
            {
                type: 'separator'
            }
        ];

        let cursos = data.buscarTodosCursos();
        cursos.forEach((curso) => {
            let menuItem = {
                label: curso,
                type: 'radio',
                click: () => {
                    mainWindow.send('curso-trocado',curso);//esse evento vai pro renderer.js
                }
            }

            template.push(menuItem);
        });
        this.templateInicial = template;
     
        return template;
    },
    adicionaCursoNoTray(curso,mainWindow){
        this.templateInicial.push({
            label: curso,
            type: 'radio',
            checked: true,
            click: () => {
                mainWindow.send('curso-trocado',curso);//esse evento vai pro renderer.js
            }
        })
        return this.templateInicial;
    } ,
    criaMenuPrincipal(mainWindow){
        let templateMenu = [
            {
                label: 'Ferramentas',
                submenu: [
                    {
                        role: 'reload'
                    },
                    {
                        role: 'toggledevtools'
                    },
                    {
                        role: 'minimize'
                    },
                    {
                        role: 'close'
                    },
                    {
                        label: 'Iniciar/Parar',
                        click:() =>{
                            mainWindow.send('inicia-timer-atalho');
                        }
                    }
            ]
           },           
            {
                label: "Sobre",
                submenu: [
                {
                    label:'Sobre a aplicação',
                    click:() =>{                  
                    ipcMain.emit('abrir-janela-sobre');
                    }
                }
                ]
          }];
      
          return templateMenu;
    } 
}